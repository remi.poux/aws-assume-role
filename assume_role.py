#!/usr/bin/python3

"""
  mfa.json:
    {
      # AWS example:
      "mfa_aws_1": {
        "serial" : "arn:aws:iam::<AWS_ACCOUNT_ID_1>:mfa/<USER_NAME_1>"
        "seed"   : [a-zA-Z0-9]{65}
      },
      "mfa_aws_2": {
        "serial" : "arn:aws:iam::<AWS_ACCOUNT_ID_2>:mfa/<USER_NAME_2>"
        "seed"   : [a-zA-Z0-9]{65}
      }
      ...
    }

  conf.json:
    {
      "root_profile"     : "my_root_profile",
      "duration_seconds" : 14400,             # AWS upper limit: 14400

      "targets" : {
        "target_id_1" : [
          "jump_id_1"
        ],
        "target_id_2" : [
          "jump_id_1",
          "jump_id_2"
        ]
      },

      "jumps" : {
        "jump_id_1" : {
          "mfa"          : "mfa_id_1",
          "arn"          : "arn:aws:iam::094052684538:role/DevOps",
          "session_name" : "devops"
        },
        "jump_id_2" : {
          "mfa"          : "",
          "arn"          : "arn:aws:iam::094052684538:role/PROD_role_kube_user",
          "session_name" : "kube-user"
        }
      }
    }
"""



import json
import os

import argparse

from boto3 import Session, client
from oathtool import generate_otp



def jumps (root_profile_session, root_profile_sts_client, conf, mfa, target):
  """
      Function that start with root profile credentials,
      and from there perform a chain of assume roles (as defined in conf and mfa params),
      in order to reach a target.

      Return: sts_response containing the credentials for the target.
  """

  sts_client = root_profile_sts_client

  for jump_name in conf['targets'][target]:
    # assume role +1

    # Get new jump to make
    jump = conf['jumps'][jump_name]

    # MFA ?
    if jump ['mfa']:
      mfa_jump  = mfa [jump ['mfa']]
      mfa_token = generate_otp (mfa_jump ['seed'])

      # Assume role WITH mfa
      sts_response = sts_client.assume_role (
              RoleArn         = jump ['arn'],
              RoleSessionName = jump ['session_name'],
              DurationSeconds = conf ['duration_seconds'],
              SerialNumber    = mfa_jump ['serial'],
              TokenCode       = mfa_token
              )
    else:
      # Assume role WITHOUT mfa
      sts_response = sts_client.assume_role (
        RoleArn         = jump['arn'],
        RoleSessionName = jump['session_name'],
        DurationSeconds = conf['duration_seconds']
        )

    # Define new client with assumed role in order to be ready for the nex step
    sts_client = client (
              'sts',
              aws_access_key_id     = sts_response ['Credentials']['AccessKeyId'],
              aws_secret_access_key = sts_response ['Credentials']['SecretAccessKey'],
              aws_session_token     = sts_response ['Credentials']['SessionToken']
              )

  return sts_response

def print_credentials_bash (sts_response, region):
  """
      Function that print bash compliant commands,
      in order to export AWS credentials as environment variables.
  """

  print ('export AWS_ACCESS_KEY_ID='     + sts_response ['Credentials']['AccessKeyId']     + ';')
  print ('export AWS_SECRET_ACCESS_KEY=' + sts_response ['Credentials']['SecretAccessKey'] + ';')
  print ('export AWS_SESSION_TOKEN='     + sts_response ['Credentials']['SessionToken']    + ';')
  print ('export AWS_DEFAULT_REGION='    + region                                          + ';')
  print ('aws sts get-caller-identity;')



if __name__ == '__main__':

  conf_filename = os.getenv ('BPI_PATH_CONF')
  mfa_filename  = os.getenv ('BPI_PATH_MFA')

  parser = argparse.ArgumentParser (description = 'Chain assume roles')
  parser.add_argument('target', help='Name of target you want to reach')
  args = parser.parse_args()

  if mfa_filename:
    with open(mfa_filename, 'r') as f:
      mfa = json.load(f)

  if conf_filename:
    with open(conf_filename, 'r') as f:
      conf = json.load(f)

  root_profile_session    = Session (profile_name = conf['root_profile'])
  root_profile_sts_client = root_profile_session.client ('sts')

  sts_response = jumps (
          root_profile_session,
          root_profile_sts_client,
          conf,
          mfa,
          args.target
          )

  print_credentials_bash (
          sts_response,
          root_profile_session.region_name
          )

