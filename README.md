# Overview

This aim of this script is to be able to perform one or more AWS `assume-roles` easily.

Each execution:

- Start by using the configured `AWS root profile`
- And then assume one role
- And then another, using the credentials that it got from the previous assume-role
- And so on
- And at the end it will print bash-compliant (at the time of writing) commands in order to export AWS credential-related en vars to that you can use the targetted role

So you can configure what we call `targets`.
A target is a destination, defined as a role assumed in an AWS account
(e.g. role devops in prod account is a possible target)

A target is specified in a conf as a succession of `jumps`.
A jump is always a single assume-role.

In other words, a `target` is a possibily multiple assume-role (without predefined limit),
that is reached using a succession of single assume-role called `jumps`.

The [config file](## Config file) makes you able to configure the root profile, the named targets, all possible jumps and optionnaly MFA usage.

It is also possible to share jumps definitions between targets.


The wole idea of this script is focus on which target you want to use,
and not whether you need to perform 3 assume-roles and 2 different MFAs in order to reach it :-)


# Initial warning

## About environment variables

AWS STS credentials are used through the definition of the following environment variables in your terminal:

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_SESSION_TOKEN
- AWS_DEFAULT_REGION

This means you wll need to use the `assume_role` script in every terminal you want to connect to AWS with.

This also means that you shouldn't override these variables afterward.

## Python version

Although this script may easily be dapated to work with `Python2.x`,
please keep in mind that it was built and tested using `Python3`.


# Install

## Libs and sources

First of all, you need to install some libs:

```bash
# Install python3 for running the script
#     and python3-pip for dependencies intallation
apt-get install python3 python3-pip

# Install AWS Python CLI
pip3 install boto3

# Install tools lib that generate MFA token codes from the OATH_SEED
pip3 install oathtool
```

## AWS CLI root profile

Edit the AWS credentials (`~/.aws/credentials`) file in order to add the following:

```bash
[my_root_profile]
aws_access_key_id = <MY_ACCESS_KEY_ID>
aws_secret_access_key = <MY_SECRET_ACCESS_KEY>
```

And then edit the profie config file (`~/.aws/config`) in order to add:

```bash
[profile my_root_profile]
region = <MY_REGION>
```

## MFA file

Create a `mfa.json` file, for example at `~/.aws/mfa.json`, like this:

```bash
# mfa.json

{
  "mfa_aws_1": {
    "serial" : "arn:aws:iam::<AWS_ACCOUNT_ID_1>:mfa/<USER_NAME_1>"
    "seed"   : [a-zA-Z0-9]{64}
  },
  "mfa_aws_2": {
    "serial" : "arn:aws:iam::<AWS_ACCOUNT_ID_2>:mfa/<USER_NAME_2>"
    "seed"   : [a-zA-Z0-9]{64}
  }
  ...
}
```

## Config file

Define in a suited location the following config file.

**Note:** You may want to have one such file per projects.

```json
# conf.json

{
  "root_profile"     : "my_root_profile",
  "duration_seconds" : 14400,             # AWS upper limit: 14400

  "targets" : {
    "target_id_1" : [
      "jump_id_1"
    ],
    "target_id_2" : [
      "jump_id_1",
      "jump_id_2"
    ]
  },

  "jumps" : {
    "jump_id_1" : {
      "mfa"          : "mfa_id_1",
      "arn"          : "arn:aws:iam::094052684538:role/DevOps",
      "session_name" : "devops"
    },
    "jump_id_2" : {
      "mfa"          : "",
      "arn"          : "arn:aws:iam::094052684538:role/PROD_role_kube_user",
      "session_name" : "kube-user"
    }
  }
}
```

## Config files paths env vars

The script read the location of its config file through some environment variables,
that therefore need to be defined for the script to work.

### Mono-project

Add the following for example at the end of your `~/.bashrc` or `~/.zshrc`:

```bash
export ASSUME_ROLE_PATH_CONF="<ABSOLUTE_PATH_TO_CONF>"
export ASSUME_ROLE_PATH_MFA="/home/<USER>/.aws/mfa.json"
```

Then restart your terminal or source the appropriate file.

### Multi-project

**Note:** `direnv` is a useful tool when you want to manage environment variables by project, using a working root dir for each project (for exemple under `/projects/project_<MY_PROJECT>`).

Install `direnv`:

```bash
apt-get install direnv
```

Then write your env vars into a `.envrc` file:

```bash
export ASSUME_ROLE_PATH_CONF=""<ABSOLUTE_PATH_TO_CONF>
export ASSUME_ROLE_PATH_MFA="/home/<USER>/.aws/mfa.json"
```

And then tell `direnv` to allow sourcing your `.envrc` :

```bash
cd /projects/project_<MY_PROJECT>
direnv allow
```

From now on, every time you go in this directory, or in any of its sub-directory, then `direnv` will load these variables.

**Careful:** The env vars are loaded when entering the directory and can still be overriden afterward.

## Symbolic link

Finally, get the script and create a link so that you'll be able to use it from anywhere:

```bash
git clone <GIT_URL> /opt/aws_assume_role
cd /opt/aws_assume_role/
ln -s /opt/aws_assume_role/assume_role.py /usr/bin/assume_role
```

## Try it

You can use the script in order to get new credentials like this.

The following command will print bash commands containing env var exports.

```bash
assume_role my_target
```

You then need to execute those commands in your terminal.

When it's done, try `aws sts get-caller-identity` or any other aws commands,
you should now have your newly assumed credentials.

## Aliases

As you just so it, using the script imply to first run it and then source / eval its results.

I therefore recommand the use of aliases defined like so:

```bash
alias assume_my_target="eval \$(assume_role my_target)"
```

Then restart your terminal or source the appropriate file.

From now, just using `assume_my_target` command will both run the script and export the env vars.

To go further I recommand you to build a logic for naming this aliases, like this one:

- Always starts with `sar`, which stands for "STS Assume-Role "
- The 3rd letter is the first letter of the role to be assumed
- The last letter.s are for the targetted account

For example, if you want to assume a role named `devops` in a production account,
then you should proceed like this:

```bash
# Create the alias
alias sardpro="eval prod"

# Assume the role
sardpro

# Try your temporary credentials
aws sts get-caller-identity
```

**Note:** This naming convention is just an example. One should adapt it to fit their needs.

